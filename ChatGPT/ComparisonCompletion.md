## Comparison Completion

In the ORKG a comparison contains a set of properties and a set of values for each property. The properties are the same for all values. The values are compared to each other. The comparison is completed by adding the missing values.

We ask the model to extract values from the text of a paper given a set of properties.

The example here coming from this comparison: https://orkg.org/comparison/R189475/.

## <div style="text-align: left"> 🤓 Human </div>
**Extract the values of the following properties from this scholarly text. Be concise and display the results in a table.**

**The properties:** Research problem, implementation, method, operates on, targets, domain.

**The text:**

Scholarly Knowledge Graphs (KGs) provide a rich source of structured information representing knowledge encoded in scientific publications. 
With the sheer volume of published scientific literature comprising a plethora of inhomogeneous entities and relations to describe scientific concepts, these KGs are inherently incomplete. 
We present exBERT, a method for leveraging pre-trained transformer language models to perform scholarly knowledge graph completion. 
We model triples of a knowledge graph as text and perform triple classification (i.e. belongs to KG or not).
The evaluation shows that exBERT outperforms other baselines on three scholarly KG completion datasets in the tasks of triple classification, link prediction, and relation prediction.
Furthermore, we present two scholarly datasets as resources for the research community, collected from public KGs and online resources.
The proliferation of scientific literature creates new challenges in the research community, such as reproducibility crisis, duplication, inefficiency, and a lack of transparency \cite{white2017science}. Thus, organizing scholarly knowledge is one of the most pressing tasks for solving current and upcoming societal challenges. 

In general, Knowledge Graphs (KGs) have become central sources to structure knowledge and facts stored in so-called triples (\textit{head}, \textit{relation}, \textit{tail}). These triples are used in downstream information extraction tasks such as entity linking, relation extraction, and question answering~\cite{jaradeh2020Question,Cui2017KBQA}. Although large-scale KGs vary in terms of scope and coverage, they are often suffering from incompleteness and sparseness~\cite{Ji2016Knowledge}. Therefore, KGs are required to be regularly updated and completed with missing information.
The incompleteness of KGs motivates the knowledge graph completion task comprising several subtasks~\cite{Yao2019kgbert,Cai2018A}: \rNum{1}) \textit{Link Prediction} aims to find missing head/tail entities in triples. \rNum{2}) \textit{Relation Prediction} predicts  missing relations between two entities. 
KG completion methods calculate the plausibility of a triple via a scoring function to determine the validity of a knowledge graph triple~\cite{Lin2015Learning}. These approaches can be broadly categorized into Knowledge Graph Embeddings (KGE) and Language Models (LM)~\cite{Wang2017Knowledge}.
KGE techniques such as TransE~\cite{Bordes2013Translating} and ConvE~\cite{dettmers2018convolutional} learn entity and relation representation in a low dimensional vector space. However, due to their limitation of utilizing semantic information from text these methods produce different representations for the same entities in distinct triples~\cite{an2018accurate}.
Thus, LM techniques for KG completion emerged in an attempt to represent semantic information encoded in texts and produce contextualized embeddings~\cite{devlin2019bert}.%~\cite{devlin2019bert,brown2020language}.

\textit{\textbf{Motivation.}}
While KGs are a known solution for representing and managing for encyclopedic and common sense knowledge, e.g., in DBpedia~\cite{auer2007dbpedia} and
Wikidata~\cite{Vrande2014Wikidata}, the usage of KGs for scholarly knowledge is a rather new approach. Recently, researchers have focused on building scholarly KGs such as the Open Research Knowledge Graph~\cite{Jaradeh2019Open} and MAG~\cite{farber2019microsoft} to mitigate these previously mentioned challenges and to adhere to the FAIR data principles~\cite{wilkinson2016fair}. In contrast to the typical usage of KGs for encyclopedic and common sense knowledge, managing scholarly knowledge is significantly more challenging due to the heterogeneity, in-homogeneity and evolution of scholarly communication~\cite{hey2009fourth}.
%Figure~\ref{fig:teaser} illustrates scholarly KG completion tasks using three systems on a few concrete examples.
Scholarly KGs differ from encyclopedic purpose KGs because the encoded information is derived from the scholarly literature~\cite{Dessi2020AIKG}.
As a result, the scholarly KGs are ordinarily sparser than generic KGs because structured scholarly knowledge is more laborious to extract~\cite{Jaradeh2019Open}.
Furthermore, these KGs are ambiguous due to a lack of standard terminology used across the literature and poses domain-specific challenges for KG completion task.
Due to domain-specific challenges, the performance of existing KG completion methods such as KG-BERT and TransE are limited when applied to scholarly KG completion tasks (%\cf \autoref{fig:teaser} and 
Section~\ref{sec:eval}).
The observed behavior is not surprising and is due to the peculiar entity and relation types of scholarly KGs.
%For example, background knowledge is required to understand entities and relations of scholarly KGs in \autoref{fig:flow}. 
Hence, we suggest that existing KG completion approaches require additional task-specific context for the scholarly domain.
We focus on the task of KG completion for the scholarly domain.
Inspired by recent advancements in contextualizing language models~\cite{mulang2020evaluating}, we argue that LMs can be utilized for scholarly KG completion tasks if fed with context derived from the scholarly literature.
Our rationale for the choice is as follows: Language models such as SciBERT~\cite{Beltagy2019Scibert} are already trained in an unsupervised manner on large corpora of scholarly literature.
Hence, adapting SciBERT for KG completion will allow us to inherit task-specific context. 
As such, we investigate the following research question:
	\RQN{1}{What is the impact of task-specific context on scholarly KG completion?}


\textit{\textbf{Approach and Contribution.}}
We model KG completion as a sequence classification problem by treating triples of the scholarly KG as sequences of natural language text.
We propose exBERT, a contextualized approach for scholarly KG completion.
In exBERT, we build on SciBERT~\cite{Beltagy2019Scibert} and adapt the underlying pre-trained language model on augmented sequences - augmented by appending types and synonyms of labels for triple elements - for predicting the plausibility of a triple, relation, or an entity.
Our empirical results on the scholarly KG completion datasets provide superior performance on all three KG completion sub-tasks.

We provide the following three key contributions:
\begin{enumerate}[leftmargin=0.18cm]
    \item[\rNum{1})] exBERT a system that performs scholarly knowledge graph completion task, including the subtasks of triple classification, relation prediction, and link prediction.
    \item[\rNum{2})] Extensive evaluation on several datasets to show that proposed method achieves better results against various baselines for the respective tasks. Our proposed datasets, and code are publicly available\footnotemark[1].
    \item[\rNum{3})] We release two publicly available scholarly datasets\footnotemark[1] curated from scholarly KGs for the KG completion task. 
\end{enumerate}




## <div style="text-align: right"> AI 🤖 </div>

| Property | Value |
| --- | --- |
| Research problem | Incompleteness of scholarly knowledge graphs (KGs)|
| Implementation |	exBERT |
|Method|	Leveraging pre-trained transformer language models to perform scholarly knowledge graph completion via triple classification
|Operates on|	Scholarly KGs, triples of a knowledge graph
|Targets|	Triple classification, link prediction, relation prediction
|Domain|	Scholarly knowledge, scientific publications


## Alternative
You can ask the model to extract not only a single value but multiple values to be used in a suggestion usecase.

So the prompt would be something like:

## <div style="text-align: left"> 🤓 Human </div>
**Extract three possible values of each the following properties from this scholarly text. Be concise and display the results in a table (Property, Value1, Value2, Value3).**

**The properties:** Research problem, implementation, method, operates on, targets, domain.

**The text:**

Text here .... (copy from above!)

## <div style="text-align: right"> AI 🤖 </div>

| Property | Value1 | Value2 | Value3 |
| --- | --- | --- | --- |
| Research problem | Incompleteness of Scholarly Knowledge Graphs | Challenges in managing scholarly knowledge | Limited performance of existing KG completion methods on scholarly KGs |
| Implementation | exBERT | Transformer language models | Triple classification |
| Method | KG completion | Triple classification | Link prediction and relation prediction |
| Operates on | Scholarly Knowledge Graphs | Scientific literature | Triples of knowledge graph |
| Targets | Solving societal challenges | Organizing scholarly knowledge | Updating and completing KGs with missing information |
| Domain | Scholarly knowledge | Scientific literature | Scholarly KGs |



