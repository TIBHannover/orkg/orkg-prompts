# ORKG Prompts

A safe place to store any interesting prompts (usually for LLMs like GPT3) with their output so they can be replicated or used in other usecases by the ORKG team.

## Table of Content
* [ChatGPT](#chatgpt) prompts
    + Information Extraction
    + Comparison Completion


## ChatGPT
A collection of prompts that can be used with [OpenAI's ChatGPT](https://chat.openai.com/)
* [ChatGPT/IE](/ChatGPT/IE.md) - Extract a table of predicate-value pair from paper text, and then can be used to further structure lengthy textual values.
* [ChatGPT/CompCompletion](/ChatGPT/ComparisonCompletion.md) - Given a set of predicate, extract the values of those predicates from a paper text.